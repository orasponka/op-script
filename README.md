# OP Installation Script

OP Installation Script is a script that installs packages that I have found useful. <br>
Script most often uses pacman to install packages. <br>
Script isntalls some my packages and AUR Packages from [my own Ach Linux Repository](https://gitlab.com/orasponka/op-arch-repo). <br>
Works only with Arch Linux and Arch Linux based distros. Tested and made on ArcoLinux. <br>
This project is little bit inspired from DT's [dtos project](https://gitlab.com/dwt1/dtos).

Script is intended for my personal use. For example if i distro hop i can run this installation script. <br>
Script may be unstable and may not work on all devices or distros, so use script at your own risk.

## Usage
```
$ git clone https://gitlab.com/orasponka/op-script.git 
$ cd op-script/
$ chmod +x op-script
$ ./op-script
```
## Phases
Script has phases. In different phases script does different things. 
All phases and what they do is listed below <br>
You can decide which phases you to do and what to install during script run.

1. Packages <br>
In this phase script installs all packages listed on [low-pacman-pkglist.txt](https://gitlab.com/orasponka/op-script/-/blob/main/low-pacman-pkglist.txt) or [high-pacman-pkglist.txt](https://gitlab.com/orasponka/op-script/-/blob/main/high-pacman-pkglist.txt)<br>
And also in this phase script enable [op-arch-repo](https://gitlab.com/orasponka/op-arch-repo). <br>
This phase may took most time of all these phases.

2. Sddm Theme <br>
In this phase script can change sddm theme to [multicolor-sddm-theme](https://gitlab.com/dwt1/multicolor-sddm-theme)

3. Configs <br>
In this phase script can download [mine configs](https://gitlab.com/orasponka/dotfiles) for AwesomeWM, Alacritty, Starship, Vim and other apps.

4. Wallpapers <br>
In this phase script downloads wallpapers from [dt's wallpaper repository](https://gitlab.com/dwt1/wallpapers) <br>
This phase can took few minutes

5. Games <br>
In this phase script installs games like [0 A.D.](https://play0ad.com/) and [Sauerbraten](http://sauerbraten.org/).

6. DOOM Emacs (May not always work) <br>
In this phase script installs [DOOM Emacs](https://github.com/hlissner/doom-emacs) with [my configs](https://gitlab.com/orasponka/dotfiles) 

7. Icons <br>
Installs some icons for systray.
